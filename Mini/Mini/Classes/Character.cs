﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini.Classes
{
    public class Character
    {
        public Category Name;
        public int Live;
        public int Magic;
        public int Level;
        public int Defense;
        public int Speed;
        public List<Spell> spells;
        private Spell _selectedspell;
        

        public Character(Category name, int live, int magic, int level, int defense, int speed)
        {
            Name = name;
            Live = live;
            Magic = magic;
            Level = level;
            Defense = defense;
            Speed = speed;
            spells = new List<Spell>();
        }

        public void SelectSpell(IEnumerable<Character> PotentialTarget )
        {
            if(_selectedspell == null)
            {
                _selectedspell = spells.FirstOrDefault();
                _selectedspell.Target = PotentialTarget.FirstOrDefault(c => this != c);
            }
        }

        

        public void UseSpell()
        {
            
            _selectedspell.Use();
            _selectedspell = null;
        }
    }
}
