﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini.Classes
{
    public class Turn
    {
        private Character _characterOne;
        private Character _characterTwo;
        private Character[] _characters; 
        private Spell _spellOne;
        private Spell _spellTwo;


        public Turn(params Character[] fighters )
        {
            _characters = fighters;

        }

        public void Start()
        {
            // selection du sort 
            //Todo automatiqer ça après

            var characters  =  _characters.ToList().OrderBy(c => c.Speed);

            foreach (var C in characters)
            {
                C.SelectSpell(characters);
            }
            // les attaques 
            foreach (var C in characters)
            {
                C.UseSpell();
            }
            
        }
    }
}
