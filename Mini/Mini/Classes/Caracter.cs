﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini.Classes
{
    // constructeur des personnage (monstre, joueur ect) 
    public class Caracter
    {
        public string Name;
        public int Live;
        public int Magic;
        public int Level;
        public int Defense;
        public int Speed;

        public Caracter(string name,  int live, int magic, int level, int defense, int speed)
        {
            Name = name;
            Live = live;
            Magic = magic;
            Level = level;
            Defense = defense;
            Speed = speed;
            
        }
    }
}
