﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mini.Classes;

namespace Mini.Classes
{
    
    class Fight
    {
        List<Character> _characters;

        public Fight(List<Character> characters)
        {
            _characters = characters;
        }

        public void Start()
        {
            Console.WriteLine("Début du combat");
            do
            {
                //deroulement du combat 
                // ordonner les comatant par ordre de vitesse 
                _characters.OrderBy(c => c.Speed);

                //faire attaquer les combatant chacun leur tour 
                foreach (var fighter in _characters)
	            {
                    fighter.SelectSpell(_characters);
	            }

            } while (Isalive(_characters)== true);
        }

        public bool Isalive (List<Character> characters)
        {
            List<Character> aliveCharacter = new List<Character>();

            foreach (var fighter in characters)
            {
                if(fighter.Live > 0 )
                {
                    aliveCharacter.Add(fighter);
                }
            }
	      
            if (aliveCharacter.Count > 1){
                return true;}
            else 
                {return false;}                	
        }
        


    }
}
 