﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mini.Classes;

namespace Mini.Classes
{ 
    //contruceur des personnages jouables 
    public class Playable : Caracter 
    {
        public List<Spell> spells;

        public Playable(String name, int live, int magic, int level, int defense, int speed, int xp ) : base(name, live, magic, level, defense, speed)
        {
            spells = new List<Spell>();
        }
    }
}
