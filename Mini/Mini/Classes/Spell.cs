﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini.Classes
{
    public class Spell
    {
        public string SpellName;
        public int Cost;
        public int Damage;
        public Character Target
        {
            get { return Target; }
            set
            {
                Target = value;
            }
        }

        public Spell (int cost, int damage, int heal)
        {
            SpellName = spellName;
            Cost = cost;
            Damage = damage;
        }

        public void Use()
        {
            Target.Live -= Damage;
        }


    }
}
