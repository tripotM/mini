﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini.Classes
{
    class playerchoice
    {
        ConsoleDisplayer Display;

        public Character Chosecharacter(List<Character> characters)
        {
            bool isInputInteger = false;
            int indicateur; 

            for (int i = 0; i < characters.Count(); i++)
            {
                Console.WriteLine("[" + i + "]" + characters[i].ToString());
            }


            isInputInteger = false;
            do
            {

                Console.WriteLine("Quel personnage voulez vous prendre ?:");
                isInputInteger = int.TryParse(Console.ReadLine(), out indicateur);
            }
            while (!isInputInteger);


            return characters[indicateur];
        }
    }
}
