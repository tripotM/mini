﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini.Classes
{
    class ConsoleDisplayer
    {
        // Affichage Selection des personnages
        public void CharacterListDisplayer(List<Character>characters)
        {
            int i = 0; 
            foreach (var c in characters)
            {
                Console.WriteLine("["+i+"]" + "[" + c.Name + "]" ); 
            }
        }

    }
}
